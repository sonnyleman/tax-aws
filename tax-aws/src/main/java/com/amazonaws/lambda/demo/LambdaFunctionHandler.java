package com.amazonaws.lambda.demo;

import com.akatia.tax.calc.draft.TaxCalculator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

//public class LambdaFunctionHandler implements RequestHandler<S3Event, String> {
//public class LambdaFunctionHandler implements RequestHandler<TaxCalcRequestClass, Context> {
//public class LambdaFunctionHandler implements RequestHandler<String, Context> {
public class LambdaFunctionHandler  {
// private AmazonS3 s3 = AmazonS3ClientBuilder.standard().build();
    
    public LambdaFunctionHandler() {}

//    @Override
    public String handleRequest(String requestParam, Context context) {
        //context.getLogger().log("Received request: " + request);
        //context.getLogger().log("Received request.sender: " + request.sender);

        context.getLogger().log("Received request.param: " + requestParam);
		System.out.println("handleRequest  requestParam: " + requestParam);

        try {
        	String response = TaxCalculator.calculateTax(requestParam);
    		System.out.println("handleRequest  response: " + response);
        	
            return response;
        } catch (Exception e) {
    		System.out.println("handleRequest exception.message: " + e.getMessage());
    		System.out.println("handleRequest exception.toString: " + e.toString());
            e.printStackTrace();
            context.getLogger().log(String.format(
                "Error getting response for param %s" 
                , requestParam));
            throw e;
        }
    }
    
}