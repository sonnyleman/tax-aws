/**
 * @author Sonny Leman
 * This static class keep the information read the configuration properties
 * 
 */


package com.akatia.tax.calc.draft;

import java.util.Properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PropertyReader {
	private static final String PROPERTY_FOLDER = "config"; // property/configuration subfolder
	private static String propertiesFileName = "taxjar.properties";
	private static final String ERROR_CONFIGURATION_NOT_FOUND = "Configuration file/folder is not found";
	
	private static Properties currentProperties;
	
	public static Properties getCurrentProperties() 
	{
		if (currentProperties == null) {
			currentProperties = readConfiguration();
		}
		return currentProperties;
	}
	
	// ========================================================================
	// readConfiguration should be called once at the beginning of the program
	public static Properties readConfiguration() {
		System.out.println( "PropertyReader.readConfiguration starts");
		System.out.println(propertiesFileName);
		
		Path propertyFolderPath = Paths.get(PROPERTY_FOLDER);
		String absolutePropertyPathString = propertyFolderPath.toAbsolutePath().toString();
		System.out.println("Current property path is: " + absolutePropertyPathString);		
		String fileCompletePathName = absolutePropertyPathString + "/" + propertiesFileName;
		
		Path propertyFilePath = Paths.get( PROPERTY_FOLDER, propertiesFileName);
		if (Files.notExists(propertyFilePath)) 
		{
			// 
			System.out.println( ERROR_CONFIGURATION_NOT_FOUND + propertyFilePath.toAbsolutePath());
			return null;
		} 
		
		
		//System.out.println( "path exist: " +  configurationFilePath.);
		Properties props = new Properties();
		
		try{
			
			props.load(new FileInputStream( fileCompletePathName ));
			System.out.println( "Configuration file found: " + fileCompletePathName );
			
			// map parameters to class properties
			// serverAddress = props.getProperty("serverAddress");
			// userName = props.getProperty("userName");
			
			//currentProperties.setProperty('apiToken', '');
			
		} catch (FileNotFoundException ex) {
			System.out.println( "Configuration file not found: " + fileCompletePathName );
			ex.printStackTrace();
		} catch (Exception ex) {
			// assume default configuration for properties
			System.out.println( "PropertyReader unexpected exception for: " + fileCompletePathName );
			ex.printStackTrace();
		}
		
		return currentProperties;
	} // readConfiguration
}
