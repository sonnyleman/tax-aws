/**
 * 
 */
package com.akatia.tax.calc.draft;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

// the following might be better move to the detaisl
import com.taxjar.Taxjar;
import com.taxjar.exception.TaxjarException;
import com.taxjar.model.taxes.TaxResponse;

import org.json.JSONArray;
import org.json.JSONObject;

//import com.google.gson;
import com.google.gson.Gson; //.SerializedName;

/**
 * @author sonny leman
 *
 */
public class TaxCalculator {
	
	// Properties properties;
	static final String SMART_CALCS_API_TOKEN = "d3c614e807571ce5f5f33d5ef95bd39b";
	
	// ========================================================================
	// input to be defined later
	// addresses for header
	// line items
	static public String calculateTax(String jsonMsgString) {
		// get property settings
		// Properties properties = PropertyReader.getCurrentProperties();
		
		// build tax jar parameters 
		// String jsonMsgString = "{\"to_zip\":\"28209\",\"to_street\":\"5501 Carnegie Blvd\",\"to_state\":\"NC\",\"to_country\":\"US\",\"to_city\":\"Charlotte\",\"shipping\":12.0,\"nexus_addresses\":[{\"zip\":\"28209\",\"street\":\"5501 Carnegie Bldv\",\"state\":\"NC\",\"country\":\"US\",\"city\":\"Charlotte\"}],\"line_items\":[{\"unit_price\":125,\"quantity\":2,\"product_tax_code\":\"1001\",\"id\":1,\"discount\":23}],\"from_zip\":\"28209\",\"from_street\":\"5501 Carnegie Blvd\",\"from_state\":\"NC\",\"from_country\":\"US\",\"from_city\":\"Charlotte\",\"discount\":5.0,\"amount\":100.0}";		
		Map<String, Object> params = getParams(jsonMsgString);
		
		// send to taxjar
		Taxjar client = new Taxjar( SMART_CALCS_API_TOKEN );
		try {
			TaxResponse res = client.taxForOrder(params);
			System.out.println( "calculateTax: res:" + res);
			System.out.println("calculateTax: res.tax: " + res.tax);
			System.out.println("calculateTax: res.tax.getOrderTotalAmount: " + res.tax.getOrderTotalAmount());
			System.out.println("calculateTax: res.tax.getShipping: " + res.tax.getShipping());
			System.out.println("calculateTax: res.tax.getTaxableAmount: " + res.tax.getTaxableAmount());
			System.out.println("calculateTax: res.tax.getRate: " + res.tax.getRate());
			System.out.println("calculateTax: res.tax.amountToCollect: " + res.tax.getAmountToCollect());
			System.out.println("calculateTax: res.tax.hasNexus: " + res.tax.getHasNexus());
			//System.out.println("res.tax.getBreakdown: " + res.tax.getBreakdown());
			
			
			
			Gson gson = new Gson();
			String jsonResponse = gson.toJson(res);
			System.out.println( "jsonResponse: jsonReponse:" + jsonResponse);
			
			return jsonResponse ;
			// convert return TaxResponse to JSON
		} catch (TaxjarException e) {
			System.out.println("calculateTax: exception");
			e.printStackTrace();
			Gson gson = new Gson();
			String jsonResponse = gson.toJson(e);
			System.out.println("calculateTax: exception jsonResponse:" + jsonResponse);
			return jsonResponse; 
		}
		
		// return to lambda
		
	}
	
	
	// ========================================================================
	// this convert json parameters sent by SFDC to AWS lambda to calculate tax
	private static Map<String,Object> getParams(String jsonMsgString) {
		// salesTransaction
		//String jsonMsgString = "{\"to_zip\":\"28209\",\"to_street\":\"5501 Carnegie Blvd\",\"to_state\":\"NC\",\"to_country\":\"US\",\"to_city\":\"Charlotte\",\"shipping\":12.0,\"nexus_addresses\":[{\"zip\":\"28209\",\"street\":\"5501 Carnegie Bldv\",\"state\":\"NC\",\"country\":\"US\",\"city\":\"Charlotte\"}],\"line_items\":[{\"unit_price\":125,\"quantity\":2,\"product_tax_code\":\"1001\",\"id\":1,\"discount\":23}],\"from_zip\":\"28209\",\"from_street\":\"5501 Carnegie Blvd\",\"from_state\":\"NC\",\"from_country\":\"US\",\"from_city\":\"Charlotte\",\"discount\":5.0,\"amount\":100.0}";
		
		JSONObject jsonParam = new JSONObject(jsonMsgString);
		
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("from_country", jsonParam.get("from_country"));
		params.put("from_state", jsonParam.get("from_state"));
		params.put("from_zip", jsonParam.get("from_zip"));
		params.put("from_city", jsonParam.get("from_city"));
		params.put("from_street", jsonParam.get("from_street"));
		
		params.put ("to_country", jsonParam.get("to_country"));
		params.put ("to_state", jsonParam.get("to_state"));
		params.put ("to_zip", jsonParam.get("to_zip"));
		params.put ("to_city", jsonParam.get("to_city"));
		params.put ("to_street", jsonParam.get("to_street"));
		
		params.put("amount", jsonParam.get("amount"));
		params.put("shipping", jsonParam.get("shipping"));
		
		//String x = (String) jsonParam.get("line_items");
		//System.out.println("x:" + x);
		
		// *
		JSONArray jsonLineItemArray = (JSONArray) jsonParam.get("line_items");
		JSONObject jsonObj = null;
		//Integer liRowIdx=0;
		
		List<Map> lstLineItems = new ArrayList();
		for (Integer rowIdx=0; rowIdx < jsonLineItemArray.length(); rowIdx++) {
			jsonObj = jsonLineItemArray.getJSONObject(rowIdx);
			Map<String, Object> lineItem = new HashMap<String,Object>();
			lineItem.put( "id", jsonObj.get("id"));
			lineItem.put( "quantity", jsonObj.get("quantity"));
			lineItem.put( "product_tax_code", jsonObj.get("product_tax_code"));
			lineItem.put( "unit_price", jsonObj.get("unit_price"));
			lineItem.put( "discount", jsonObj.get("discount"));
			lstLineItems.add(lineItem);
		}
		params.put( "line_items", lstLineItems);
		
		//JSONArray jsonNexusAddressArray = new JSONArray((String) jsonParam.get("nexus_addresses"));
		JSONArray jsonNexusAddressArray = (JSONArray) jsonParam.get("nexus_addresses");
		List<Map> lstNexusAddresses = new ArrayList();
		for (Integer rowIdx=0; rowIdx < jsonNexusAddressArray.length(); rowIdx++) {
			jsonObj = jsonNexusAddressArray.getJSONObject(rowIdx);
			Map<String, Object> nexusAddress = new HashMap<String,Object>();
			nexusAddress.put("country", jsonObj.get("country"));
			nexusAddress.put("state", jsonObj.get("state"));
			nexusAddress.put("zip", jsonObj.get("zip"));
			nexusAddress.put("city", jsonObj.get("city"));
			nexusAddress.put("street", jsonObj.get("street"));
			lstNexusAddresses.add(nexusAddress);
		}
		params.put( "nexus_addresses", lstNexusAddresses);
		
		return params;
	}
	
	
	// ========================================================================
	// this is for testing only
	/*
	public static void main(String[] args){
		System.out.println("TaxCalculator main starts");
		String jsonMsgString = "{\"to_zip\":\"28209\",\"to_street\":\"5501 Carnegie Blvd\",\"to_state\":\"NC\",\"to_country\":\"US\",\"to_city\":\"Charlotte\",\"shipping\":12.0,\"nexus_addresses\":[{\"zip\":\"28209\",\"street\":\"5501 Carnegie Bldv\",\"state\":\"NC\",\"country\":\"US\",\"city\":\"Charlotte\"}],\"line_items\":[{\"unit_price\":125,\"quantity\":2,\"product_tax_code\":\"1001\",\"id\":1,\"discount\":23}],\"from_zip\":\"28209\",\"from_street\":\"5501 Carnegie Blvd\",\"from_state\":\"NC\",\"from_country\":\"US\",\"from_city\":\"Charlotte\",\"discount\":5.0,\"amount\":100.0}";		
		calculateTax(jsonMsgString);
		System.out.println("TaxCalculator main ends");
	}
	
	*/
}
